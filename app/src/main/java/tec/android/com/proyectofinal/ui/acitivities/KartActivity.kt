package tec.android.com.proyectofinal.ui.acitivities

import android.support.v4.app.Fragment
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_base.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.ui.fragments.KartFragment

class KartActivity : BaseActivity() {

    override fun getFragment(): Fragment {
        return KartFragment()
    }

    override fun initView() {
        super.initView()
        setupToolbar()
    }

    private fun setupToolbar() {
        setSupportActionBar(this.toolbar)
        supportActionBar?.let { actionBar ->
            actionBar.title = "My orders"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back)
        }
        this.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

}