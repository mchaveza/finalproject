package tec.android.com.proyectofinal.ui.adapters;

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ia.mchaveza.kotlin_library.invisible
import kotlinx.android.synthetic.main.item_games.view.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.models.game_description.GameDescriptionResponse
import tec.android.com.proyectofinal.ui.utils.loadUrlFitCenter

class GamesAdapter(private val mList: MutableList<GameDescriptionResponse?>,
                   private val mListener: GameCallback) : RecyclerView.Adapter<GamesAdapter.ItemViewHolder>() {

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItems(mList[position]!!, mListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_games, parent, false))

    override fun getItemCount() = mList.size

    fun setGames(list: MutableList<GameDescriptionResponse?>) {
        mList.clear()
        mList.addAll(list)
        notifyDataSetChanged()
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: GameDescriptionResponse, listener: GameCallback) = with(itemView) {
            item_game_poster.loadUrlFitCenter(item.cover.url)
            item_game_name.text = item.name
            item_game_rating.text = String.format(context.getString(R.string.home_rating), item.esrb.rating)
            item_game_price.text = String.format(context.getString(R.string.home_pricing), item.price)
            if (item.esrb.synopsis.isNotEmpty()) {
                item_game_synopsis.text = String.format(context.getString(R.string.home_synopsis), item.esrb.synopsis)
            } else {
                item_game_synopsis.invisible()
            }

            item_game_add_kart.setOnClickListener {
                listener.onAddToKart(item)
            }
        }
    }

    interface GameCallback {
        fun onAddToKart(item: GameDescriptionResponse)
    }
}