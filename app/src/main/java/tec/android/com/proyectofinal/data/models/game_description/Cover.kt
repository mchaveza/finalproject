package tec.android.com.proyectofinal.data.models.game_description

import com.google.gson.annotations.SerializedName

data class Cover(

        @field:SerializedName("width")
        var _width: Int? = null,

        @field:SerializedName("cloudinary_id")
        var _cloudinaryId: String? = null,

        @field:SerializedName("url")
        private var _url: String? = null,

        @field:SerializedName("height")
        private var _height: Int? = null
) {

    val width: Int
        get() = _width ?: -1

    val height: Int
        get() = _height ?: -1

    val cloudinaryId: String
        get() = _cloudinaryId ?: ""

    val url: String
        get() = _url?.replace("//", "https://") ?: ""

}