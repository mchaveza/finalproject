package tec.android.com.proyectofinal.ui.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import tec.android.com.proyectofinal.R

fun ImageView.loadUrl(url: String) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions.centerCropTransform()
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}

fun ImageView.loadUrlFitCenter(url: String) {
    Glide.with(context)
            .load(url)
            .apply(RequestOptions.fitCenterTransform()
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
}