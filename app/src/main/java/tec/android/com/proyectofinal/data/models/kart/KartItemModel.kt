package tec.android.com.proyectofinal.data.models.kart

import android.os.Parcel
import android.os.Parcelable

data class KartItemModel(

        var id: Long? = -1L,

        var name: String? = "",

        var price: Long? = -1L,

        var rating: Long? = -1L,

        var url: String? = ""

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString(),
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeValue(price)
        parcel.writeValue(rating)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KartItemModel> {
        override fun createFromParcel(parcel: Parcel): KartItemModel {
            return KartItemModel(parcel)
        }

        override fun newArray(size: Int): Array<KartItemModel?> {
            return arrayOfNulls(size)
        }
    }
}