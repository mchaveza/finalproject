package tec.android.com.proyectofinal.ui.acitivities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_login.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.ui.utils.createDialog
import java.util.*

class LoginActivity : AppCompatActivity() {

    private lateinit var callbackManager: CallbackManager

    companion object {
        const val EMAIL = "email"
        const val PUBLIC_PROFILE = "public_profile"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login_button.setReadPermissions(Arrays.asList(EMAIL, PUBLIC_PROFILE))

        callbackManager = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                        finish()
                    }

                    override fun onCancel() {
                        createDialog(
                                context = this@LoginActivity,
                                title = "Error",
                                message = "Something went wrong",
                                okBtn = "Aceptar",
                                positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                                    dialog.dismiss()
                                }
                        )
                    }

                    override fun onError(exception: FacebookException) {
                        createDialog(
                                context = this@LoginActivity,
                                title = "Error",
                                message = exception.message.toString(),
                                okBtn = "Aceptar",
                                positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                                    dialog.dismiss()
                                }
                        )
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}