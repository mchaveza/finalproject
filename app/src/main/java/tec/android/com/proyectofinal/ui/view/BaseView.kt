package tec.android.com.proyectofinal.ui.view

interface BaseView {
    fun showLoading()
    fun hideLoading()
}