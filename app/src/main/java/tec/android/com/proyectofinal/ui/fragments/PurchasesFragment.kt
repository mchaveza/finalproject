package tec.android.com.proyectofinal.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.ia.mchaveza.kotlin_library.visible
import kotlinx.android.synthetic.main.fragment_purchases.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.adapters.PurchaseAdapter
import tec.android.com.proyectofinal.ui.utils.FirebaseDatabaseManager
import tec.android.com.proyectofinal.ui.utils.PurchaseDatabaseCallback

class PurchasesFragment : BaseFragment(), PurchaseDatabaseCallback {

    private lateinit var purchaseAdapter: PurchaseAdapter
    private lateinit var mDatabase: FirebaseDatabaseManager

    override fun getLayout(): Int {
        return R.layout.fragment_purchases
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mDatabase = FirebaseDatabaseManager(activity!!)
        purchaseAdapter = PurchaseAdapter(ArrayList())
        this.purchases_list.adapter = purchaseAdapter
        this.purchases_list.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        this.purchases_list.hasFixedSize()
    }

    override fun onResume() {
        super.onResume()
        mDatabase.getPurchasesData(this)
    }

    override fun onTransactionCompleted(isCompleted: Boolean) {
        return Unit
    }

    override fun onGetPurchasesItems(items: MutableList<KartItemModel>) {
        if (items.size > 0) {
            purchaseAdapter.addItems(items)
        } else {
            this.purchases_empty_state.visible()
        }
    }
}