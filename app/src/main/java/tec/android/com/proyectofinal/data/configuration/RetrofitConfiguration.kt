package tec.android.com.proyectofinal.data.configuration

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.interfaces.IgdbAPI
import tec.android.com.proyectofinal.data.interfaces.MockAPI
import tec.android.com.proyectofinal.data.models.game_description.GameDescriptionResponse
import tec.android.com.proyectofinal.data.models.genre.GenreResponse
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.data.models.search_game.SearchGameResponse
import java.util.concurrent.TimeUnit

class RetrofitConfiguration(context: Context) {

    private val gson: Gson
    private val mContext = context
    private val httpClient: OkHttpClient.Builder
    private lateinit var gamesAPI: IgdbAPI
    private lateinit var mockAPI: MockAPI
    private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor()

    init {
        logging.level = HttpLoggingInterceptor.Level.BODY

        httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(15, TimeUnit.SECONDS)
        httpClient.connectTimeout(15, TimeUnit.SECONDS)
        httpClient.writeTimeout(15, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging)

        gson = GsonBuilder().setLenient().create()
    }

    //Define which retrofit interface you're gonna be using
    fun setupIgdbAPI(endpoint: String) {
        val retrofit = Retrofit.Builder()
                .baseUrl(endpoint)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build()

        gamesAPI = retrofit.create<IgdbAPI>(IgdbAPI::class.java)
    }

    fun setupMockAPI(endpoint: String) {
        val retrofit = Retrofit.Builder()
                .baseUrl(endpoint)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build()

        mockAPI = retrofit.create<MockAPI>(MockAPI::class.java)
    }

    fun getGameId(query: String): Observable<MutableList<SearchGameResponse?>> =
            gamesAPI.getGameId(mContext.getString(R.string.igdb_api_key), query)

    fun getGameGenre(genres: List<Int>): Observable<MutableList<GenreResponse?>> =
            gamesAPI.getGameGenre(mContext.getString(R.string.igdb_api_key), genres, arrayListOf("id,name,url"))

    fun getGameDescription(gameId: Int): Observable<MutableList<GameDescriptionResponse?>> =
            gamesAPI.getGameDescription(mContext.getString(R.string.igdb_api_key), gameId, arrayListOf("id,cover,genres,esrb,name"))

    fun getFeaturedGames(): Observable<MutableList<KartItemModel?>> =
            mockAPI.getFeaturedGames()


}