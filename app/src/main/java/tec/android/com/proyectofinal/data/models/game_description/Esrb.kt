package tec.android.com.proyectofinal.data.models.game_description

import com.google.gson.annotations.SerializedName
import com.ia.mchaveza.kotlin_library.random

data class Esrb(

        @field:SerializedName("rating")
        private var _rating: Int? = null,

        @field:SerializedName("synopsis")
        private var _synopsis: String? = null
) {

    val rating: Int
        get() = _rating ?: -1

    val synopsis: String
        get() = _synopsis ?: ""

}