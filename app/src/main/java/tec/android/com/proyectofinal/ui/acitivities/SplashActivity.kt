package tec.android.com.proyectofinal.ui.acitivities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.facebook.AccessToken
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.ui.utils.ArgumentConstants

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val preferences = SharedPreferencesManager(this@SplashActivity)

        Handler().postDelayed({
            val accessToken = AccessToken.getCurrentAccessToken()
            val isLoggedIn = accessToken != null && !accessToken.isExpired
            if (isLoggedIn) {
                preferences.setStringPreference(ArgumentConstants.CURRENT_KEY, accessToken.token)
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                finish()
            } else {
                preferences.clearPreferences(ArgumentConstants.CURRENT_KEY)
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }

        }, 1000)

    }
}