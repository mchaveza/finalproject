package tec.android.com.proyectofinal.data.models.game_description

import com.google.gson.annotations.SerializedName
import java.util.*

data class GameDescriptionResponse(

        @field:SerializedName("cover")
        var _cover: Cover? = null,

        @field:SerializedName("esrb")
        var _esrb: Esrb? = null,

        @field:SerializedName("genres")
        var _genres: List<Int>? = null,

        @field:SerializedName("name")
        var _name: String? = null,

        @field:SerializedName("id")
        var _id: Int? = null,

        var _price: Int? = null

) {

    val cover: Cover
        get() = _cover ?: Cover()

    val esrb: Esrb
        get() = _esrb ?: Esrb()

    val genres: List<Int>
        get() = _genres ?: ArrayList()

    val name: String
        get() = _name ?: ""

    val id: Int
        get() = _id ?: -1

    val price: Int
        get() = _price ?: -1

}