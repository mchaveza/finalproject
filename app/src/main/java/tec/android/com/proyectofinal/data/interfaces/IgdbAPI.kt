package tec.android.com.proyectofinal.data.interfaces

import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable
import tec.android.com.proyectofinal.data.models.game_description.GameDescriptionResponse
import tec.android.com.proyectofinal.data.models.genre.GenreResponse
import tec.android.com.proyectofinal.data.models.search_game.SearchGameResponse
import tec.android.com.proyectofinal.ui.utils.ApiConfiguration

interface IgdbAPI {

    @GET(ApiConfiguration.IGDB_SEARCH)
    fun getGameId(
            @Header("user-key") key: String,
            @Query("search") gameName: String
    ): Observable<MutableList<SearchGameResponse?>>

    @GET(ApiConfiguration.IGDB_GENRES)
    fun getGameGenre(
            @Header("user-key") key: String,
            @Path("genres_id") genresId: List<Int>,
            @Query("fields") fields: List<String>
    ): Observable<MutableList<GenreResponse?>>

    @GET(ApiConfiguration.IGDB_GAME_DESCRIPTION)
    fun getGameDescription(
            @Header("user-key") key: String,
            @Path("game_id") gameId: Int,
            @Query("fields") fields: List<String>
    ): Observable<MutableList<GameDescriptionResponse?>>


}