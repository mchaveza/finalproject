package tec.android.com.proyectofinal.ui.fragments

import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import kotlinx.android.synthetic.main.fragment_confirmation.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.adapters.PurchaseAdapter
import tec.android.com.proyectofinal.ui.utils.FirebaseDatabaseManager
import tec.android.com.proyectofinal.ui.utils.PurchaseDatabaseCallback

class ConfirmationFragment : BaseFragment(), PurchaseDatabaseCallback {

    private lateinit var purchaseAdapter: PurchaseAdapter
    private lateinit var mDatabase: FirebaseDatabaseManager

    override fun getLayout(): Int {
        return R.layout.fragment_confirmation
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mDatabase = FirebaseDatabaseManager(activity!!)
        purchaseAdapter = PurchaseAdapter(ArrayList())
        this.confirmation_list.adapter = purchaseAdapter
        this.confirmation_list.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        this.confirmation_list.hasFixedSize()

        this.confirmation_home_btn.setOnClickListener {
            activity?.finish()
        }
    }

    override fun onResume() {
        super.onResume()
        mDatabase.getPurchasesData(this)
    }

    override fun onTransactionCompleted(isCompleted: Boolean) {
        return Unit
    }

    override fun onGetPurchasesItems(items: MutableList<KartItemModel>) {
        purchaseAdapter.addItems(items)
    }

}