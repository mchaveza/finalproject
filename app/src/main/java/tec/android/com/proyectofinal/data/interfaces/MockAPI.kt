package tec.android.com.proyectofinal.data.interfaces

import retrofit2.http.GET
import rx.Observable
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.utils.ApiConfiguration

interface MockAPI {

    @GET(ApiConfiguration.MOCK_FEATURED_GAMES)
    fun getFeaturedGames(): Observable<MutableList<KartItemModel?>>

}