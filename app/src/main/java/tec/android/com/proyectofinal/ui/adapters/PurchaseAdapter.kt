package tec.android.com.proyectofinal.ui.adapters;

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.invisible
import kotlinx.android.synthetic.main.item_games.view.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.utils.loadUrlFitCenter

class PurchaseAdapter(private val mList: MutableList<KartItemModel>) : RecyclerView.Adapter<PurchaseAdapter.ItemViewHolder>() {

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItems(mList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_games, parent, false))

    override fun getItemCount() = mList.size

    fun addItems(items: MutableList<KartItemModel>) {
        mList.clear()
        mList.addAll(items)
        notifyDataSetChanged()
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(item: KartItemModel) = with(itemView) {
            item_game_poster.loadUrlFitCenter(item.url.toString())
            item_game_name.text = item.name
            item_game_rating.text = String.format(context.getString(R.string.home_rating), item.rating)
            item_game_price.text = String.format(context.getString(R.string.home_pricing), item.price)
            item_game_synopsis.invisible()
            item_game_add_kart.gone()
        }
    }


}