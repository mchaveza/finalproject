package tec.android.com.proyectofinal.ui.fragments

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.pawegio.kandroid.textWatcher
import com.pawegio.kandroid.toast
import kotlinx.android.synthetic.main.activity_main.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.models.game_description.Cover
import tec.android.com.proyectofinal.data.models.game_description.Esrb
import tec.android.com.proyectofinal.data.models.game_description.GameDescriptionResponse
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.acitivities.HomeActivity
import tec.android.com.proyectofinal.ui.adapters.GamesAdapter
import tec.android.com.proyectofinal.ui.presenters.GameDescriptionPresenter
import tec.android.com.proyectofinal.ui.utils.FirebaseDatabaseManager
import tec.android.com.proyectofinal.ui.utils.KartDatabaseCallback
import tec.android.com.proyectofinal.ui.utils.createDialog
import tec.android.com.proyectofinal.ui.utils.loadingDialog
import tec.android.com.proyectofinal.ui.view.GameDescriptionView


class HomeFragment : BaseFragment(), GameDescriptionView, GamesAdapter.GameCallback, KartDatabaseCallback {

    private lateinit var gamesAdapter: GamesAdapter
    private lateinit var gamesPresenter: GameDescriptionPresenter
    private lateinit var mDatabase: FirebaseDatabaseManager
    private var loading: ProgressDialog? = null

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mDatabase = FirebaseDatabaseManager(activity!!)

        gamesAdapter = GamesAdapter(ArrayList(), this)
        home_games_list.adapter = gamesAdapter
        home_games_list.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        home_games_list.hasFixedSize()

        gamesPresenter = GameDescriptionPresenter(activity!!, this)
        gamesPresenter.getFeaturedGames()

        home_search_btn.setOnClickListener {
            with(home_search.text.toString()) {
                if (this.isNotEmpty()) {
                    gamesPresenter.getGame(this)
                    home_header.text = String.format(getString(R.string.home_results), this)
                }
            }
        }

        home_search.textWatcher {
            onTextChanged { _, _, _, _ ->
                home_search_btn.isEnabled = !home_search.text.toString().isEmpty()
            }
        }
    }

    override fun onGetGameDescription(response: MutableList<GameDescriptionResponse?>) {
        if (response.size > 0) {
            gamesAdapter.setGames(response)
        } else {
            createDialog(
                    context = context!!,
                    title = "Error",
                    message = "No results found",
                    positiveListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
            )
        }
    }

    override fun onGetGameDescriptionError(error: Throwable) {
        createDialog(
                context = context!!,
                title = "Error",
                message = error.message.toString(),
                positiveListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
        )
    }

    override fun showLoading() {
        loading = loadingDialog(context!!)
        loading?.show()
    }

    override fun hideLoading() {
        loading?.dismiss()
    }

    override fun onAddToKart(item: GameDescriptionResponse) {
        mDatabase.addKartItem(KartItemModel(item.id.toLong(), item.name, item.price.toLong(), item.esrb.rating.toLong(), item.cover.url))
    }

    override fun onResume() {
        super.onResume()
        mDatabase.getKartData(this)
    }

    override fun onGetKartItems(items: MutableList<KartItemModel>) {
        (activity as HomeActivity).updateKartItemsCount(items.size)
    }

    override fun onItemKartAdded() {
        toast("Item added to kart")
    }

    override fun onItemKartError() {
        createDialog(
                context = context!!,
                title = "Error",
                message = "An error occurred during kart adding process",
                positiveListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
        )
    }

    override fun onGetFeaturedGames(list: MutableList<KartItemModel?>) {
        if (list.size > 0) {
            val gameList = mutableListOf<GameDescriptionResponse?>()
            list.forEach {
                val model = GameDescriptionResponse()
                model._id = it?.id?.toInt()
                model._price = it?.price?.toInt()
                model._name = it?.name
                model._cover = Cover(_url = it?.url)
                model._esrb = Esrb(_rating = it?.rating?.toInt())
                gameList.add(model)
            }
            gamesAdapter.setGames(gameList)
        } else {
            createDialog(
                    context = context!!,
                    title = "Error",
                    message = "No results found",
                    positiveListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
            )
        }
    }

}