package tec.android.com.proyectofinal.ui.acitivities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.ia.mchaveza.kotlin_library.replaceFragment
import tec.android.com.proyectofinal.R

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        supportFragmentManager.replaceFragment(R.id.container, getFragment())
        initView()
    }

    open fun initView(){

    }

    abstract fun getFragment(): Fragment
}