package tec.android.com.proyectofinal.ui.acitivities

import android.content.Intent
import android.net.Uri
import android.support.v4.app.Fragment
import com.facebook.CallbackManager
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import kotlinx.android.synthetic.main.activity_base.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.ui.fragments.ConfirmationFragment

class ConfirmationActivity : BaseActivity() {

    private lateinit var callbackManager: CallbackManager
    private lateinit var shareDialog: ShareDialog

    override fun getFragment(): Fragment {
        return ConfirmationFragment()
    }

    override fun initView() {
        super.initView()
        setupToolbar()
        displayFacebookDialog()
    }

    private fun setupToolbar() {
        setSupportActionBar(this.toolbar)
        supportActionBar?.let { actionBar ->
            actionBar.title = "Confirmation"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back)
        }
        this.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun displayFacebookDialog() {
        callbackManager = CallbackManager.Factory.create()
        shareDialog = ShareDialog(this)
        if (ShareDialog.canShow(ShareLinkContent::class.java)) {
            val linkContent = ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                    .build()
            shareDialog.show(linkContent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

}