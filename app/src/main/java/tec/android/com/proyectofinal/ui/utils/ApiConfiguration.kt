package tec.android.com.proyectofinal.ui.utils

class ApiConfiguration {

    companion object {
        const val IGDB_SEARCH = "/games/"
        /*const val IGDB_GENRES = "/genres/12,9,11?fields=id,name,url "*/
        const val IGDB_GENRES = "/genres/{genres_id}"
        const val IGDB_GAME_DESCRIPTION = "/games/{game_id}"
        const val MOCK_FEATURED_GAMES = "/v2/games"
    }

}