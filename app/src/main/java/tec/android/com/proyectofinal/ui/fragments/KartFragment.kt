package tec.android.com.proyectofinal.ui.fragments

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.pawegio.kandroid.toast
import kotlinx.android.synthetic.main.fragment_kart.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.acitivities.ConfirmationActivity
import tec.android.com.proyectofinal.ui.adapters.KartAdapter
import tec.android.com.proyectofinal.ui.utils.FirebaseDatabaseManager
import tec.android.com.proyectofinal.ui.utils.KartDatabaseCallback
import tec.android.com.proyectofinal.ui.utils.PurchaseDatabaseCallback
import tec.android.com.proyectofinal.ui.utils.createDialog

class KartFragment : BaseFragment(), KartAdapter.KartCallback, KartDatabaseCallback, PurchaseDatabaseCallback {

    private lateinit var kartAdapter: KartAdapter
    private lateinit var mDatabase: FirebaseDatabaseManager

    override fun getLayout(): Int {
        return R.layout.fragment_kart
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mDatabase = FirebaseDatabaseManager(activity!!)
        kartAdapter = KartAdapter(ArrayList(), this)
        this.kart_items_list.adapter = kartAdapter
        this.kart_items_list.layoutManager = LinearLayoutManager(context!!, LinearLayout.VERTICAL, false)
        this.kart_items_list.hasFixedSize()

        this.kart_items_checkout.setOnClickListener {
            createDialog(
                    context = context!!,
                    title = "Checkout",
                    message = "Are you ready you want to proceed with checkout?",
                    okBtn = "Ok",
                    noBtn = "Cancel",
                    positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                        mDatabase.startKartTransaction(this)
                    },
                    negativeListener = DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    }
            )
        }
    }

    override fun onResume() {
        super.onResume()
        mDatabase.getKartData(this)
    }

    override fun onGetKartItems(items: MutableList<KartItemModel>) {
        if (items.size > 0) {
            this.kart_items_container.visible()
            kartAdapter.addItems(items)
            var total = 0.0
            items.forEach {
                total += it.price!!
            }
            this.kart_items_total.text = String.format(getString(R.string.kart_total), total.toString())
        } else {
            this.kart_items_container.gone()
        }
    }

    override fun onItemKartAdded() {
        return Unit
    }

    override fun onItemKartError() {
        return Unit
    }

    override fun onItemRemovedFromKart(item: KartItemModel) {
        mDatabase.deleteKartItem(item.id.toString())
    }

    override fun onTransactionCompleted(isCompleted: Boolean) {
        if (isCompleted) {
            startActivity(Intent(activity!!, ConfirmationActivity::class.java))
            activity?.finish()
        } else {
            toast("Something went wrong")
        }
    }

    override fun onGetPurchasesItems(items: MutableList<KartItemModel>) {
        return Unit
    }

    override fun onDetach() {
        super.onDetach()
        mDatabase.stopKartListener()
    }

}