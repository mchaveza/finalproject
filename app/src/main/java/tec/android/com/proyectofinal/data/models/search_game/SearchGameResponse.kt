package tec.android.com.proyectofinal.data.models.search_game

import com.google.gson.annotations.SerializedName

data class SearchGameResponse(

        @field:SerializedName("id")
        private val _id: Int? = null

) {
    val id: Int
        get() = _id ?: -1
}