package tec.android.com.proyectofinal.ui.utils

import android.content.Context
import com.google.firebase.database.*
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import tec.android.com.proyectofinal.data.models.kart.KartItemModel

class FirebaseDatabaseManager(mContext: Context) {

    private val mDatabase = FirebaseDatabase.getInstance().reference
    private val mPreferences = SharedPreferencesManager(mContext)
    private var currentKey = ""
    private var mKartListener: KartDatabaseCallback? = null
    private var mPurchaseListener: PurchaseDatabaseCallback? = null
    private val kartList = mutableListOf<KartItemModel>()
    private val purchaseList = mutableListOf<KartItemModel>()

    init {
        initializeKey()
    }

    private fun initializeKey() {
        currentKey = mPreferences.getStringPreference(ArgumentConstants.CURRENT_KEY, "")
        if (currentKey.isEmpty()) {
            currentKey = mDatabase.push().key
            mPreferences.setStringPreference(ArgumentConstants.CURRENT_KEY, currentKey)
        }
    }

    fun addKartItem(model: KartItemModel) {
        mDatabase.child(currentKey).child("kart").push().setValue(model).addOnCompleteListener({
            if (it.isComplete) {
                mKartListener?.onItemKartAdded()
            } else {
                mKartListener?.onItemKartError()
            }
        })
    }

    fun deleteKartItem(id: String) {
        mDatabase.child(currentKey).child("kart").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                var model: KartItemModel
                p0.children.forEach {
                    model = it.getValue(KartItemModel::class.java)!!
                    if (id == model.id.toString()) {
                        mDatabase.child(currentKey).child("kart").child(it.key).removeValue()
                    }
                }
            }
        })
    }

    fun getKartData(kartListener: KartDatabaseCallback) {
        mKartListener = kartListener
        mDatabase.child(currentKey).child("kart").addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                return Unit
            }

            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
                return Unit
            }

            override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                mKartListener?.onGetKartItems(extractKartData(p0))
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                val index = kartList.indexOf(p0.getValue(KartItemModel::class.java))
                if (index != -1) {
                    kartList.removeAt(index)
                    mKartListener?.onGetKartItems(kartList)
                }
            }
        })
    }

    fun getPurchasesData(purchaseListener: PurchaseDatabaseCallback) {
        mPurchaseListener = purchaseListener
        purchaseList.clear()
        mDatabase.child(currentKey).child("purchases").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                return Unit
            }

            override fun onDataChange(p0: DataSnapshot) {
                p0.children.forEach {
                    val temporalList = it.value as MutableList<HashMap<String, Any>>
                    temporalList.forEach {
                        val model = KartItemModel(
                                it["id"].toString().toLong(),
                                it["name"].toString(),
                                it["price"].toString().toLong(),
                                it["rating"].toString().toLong(),
                                it["url"].toString()
                        )
                        purchaseList.add(model)
                    }
                }
                mPurchaseListener?.onGetPurchasesItems(purchaseList)
            }
        })
    }

    fun startKartTransaction(purchaseListener: PurchaseDatabaseCallback) {
        mPurchaseListener = purchaseListener
        mDatabase.child(currentKey).child("purchases").push().setValue(kartList).addOnCompleteListener({
            if (it.isComplete) {
                mDatabase.child(currentKey).child("kart").removeValue().addOnCompleteListener({
                    if (it.isComplete) {
                        mPurchaseListener?.onTransactionCompleted(true)
                    } else {
                        mPurchaseListener?.onTransactionCompleted(false)
                    }
                })
            } else {
                mPurchaseListener?.onTransactionCompleted(false)
            }
        })
    }

    private fun extractKartData(dataSnapshot: DataSnapshot): MutableList<KartItemModel> {
        val item = dataSnapshot.getValue(KartItemModel::class.java)
                ?: KartItemModel(-1, "na", -1, -1)
        if (kartList.indexOf(item) == -1) {
            kartList.add(item)
        }
        return kartList
    }

    fun stopKartListener() {
        mKartListener = null
    }

}

interface KartDatabaseCallback {
    fun onGetKartItems(items: MutableList<KartItemModel>)
    fun onItemKartAdded()
    fun onItemKartError()
}

interface PurchaseDatabaseCallback {
    fun onTransactionCompleted(isCompleted: Boolean)
    fun onGetPurchasesItems(items: MutableList<KartItemModel>)
}

