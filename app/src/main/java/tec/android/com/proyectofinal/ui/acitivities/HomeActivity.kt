package tec.android.com.proyectofinal.ui.acitivities

import android.content.Intent
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.gone
import kotlinx.android.synthetic.main.activity_base.*
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.ui.fragments.HomeFragment
import tec.android.com.proyectofinal.ui.utils.ArgumentConstants

class HomeActivity : BaseActivity() {

    override fun getFragment(): Fragment = HomeFragment()
    private var kartItems = 0

    override fun initView() {
        super.initView()
        setupToolbar()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.let { actionBar ->
            actionBar.title = "Home"
            actionBar.setDisplayHomeAsUpEnabled(false)
            actionBar.setHomeAsUpIndicator(R.drawable.ic_back)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        val kart = menu?.findItem(R.id.home_kart)
        val kartCount = kart?.actionView?.findViewById<TextView>(R.id.notifications_count)
        if (kartItems == 0) {
            kartCount?.gone()
        } else {
            kartCount?.text = kartItems.toString()
        }
        val item = kart?.actionView?.findViewById<View>(R.id.notifications_main_container)
        item?.setOnClickListener {
            startActivity(Intent(this@HomeActivity, KartActivity::class.java))
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.home_purchases -> {
                startActivity(Intent(this@HomeActivity, PurchasesActivity::class.java))
                true
            }
            R.id.home_logout -> {
                val preferences = SharedPreferencesManager(this@HomeActivity)
                preferences.clearPreferences(ArgumentConstants.CURRENT_KEY)
                FacebookSdk.sdkInitialize(this)
                LoginManager.getInstance().logOut()
                startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                finish()
                true
            }
            else -> {
                false
            }
        }
    }

    fun updateKartItemsCount(items: Int) {
        kartItems = items
        invalidateOptionsMenu()
    }


}
