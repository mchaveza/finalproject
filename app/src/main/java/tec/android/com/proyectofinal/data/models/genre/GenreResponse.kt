package tec.android.com.proyectofinal.data.models.genre

import com.google.gson.annotations.SerializedName

data class GenreResponse(

        @field:SerializedName("name")
        private val _name: String? = null,

        @field:SerializedName("id")
        private val _id: Int? = null,

        @field:SerializedName("url")
        private val _url: String? = null
) {

    val name: String
        get() = _name ?: ""

    val id: Int
        get() = _id ?: -1

    val url: String
        get() = _url?.replace("//", "") ?: ""

}