package tec.android.com.proyectofinal.ui.presenters

import android.content.Context
import com.ia.mchaveza.kotlin_library.random
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import tec.android.com.proyectofinal.R
import tec.android.com.proyectofinal.data.configuration.RetrofitConfiguration
import tec.android.com.proyectofinal.data.models.game_description.Esrb
import tec.android.com.proyectofinal.data.models.game_description.GameDescriptionResponse
import tec.android.com.proyectofinal.data.models.kart.KartItemModel
import tec.android.com.proyectofinal.ui.view.GameDescriptionView

class GameDescriptionPresenter(private val mContext: Context, private val mView: GameDescriptionView) {

    private val api = RetrofitConfiguration(mContext)
    private val mList = mutableListOf<GameDescriptionResponse?>()
    private val featuredGamesList = mutableListOf<KartItemModel?>()

    fun getGame(query: String) {
        mList.clear()
        mView.showLoading()
        api.setupIgdbAPI(mContext.getString(R.string.igdb_endpoint))
        api.getGameId(query)
                .flatMap { list ->
                    Observable.from(list)
                }
                .flatMap({
                    api.getGameDescription(it?.id ?: 0)
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    it.forEach {
                        it?.let {
                            it._price = (250..2000).random()
                            if (it._esrb == null) {
                                it._esrb = Esrb((1..9).random(), "")
                            }
                            mList.add(it)
                        }
                    }
                }, {
                    mView.onGetGameDescriptionError(it)
                    mView.hideLoading()
                }, {
                    mView.onGetGameDescription(mList)
                    mView.hideLoading()
                })
    }

    fun getFeaturedGames() {
        featuredGamesList.clear()
        mView.showLoading()
        api.setupMockAPI(mContext.getString(R.string.mock_endpoint))
        api.getFeaturedGames()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    featuredGamesList.addAll(it)
                }, {
                    mView.onGetGameDescriptionError(it)
                    mView.hideLoading()
                }, {
                    mView.onGetFeaturedGames(featuredGamesList)
                    mView.hideLoading()
                })
    }

}