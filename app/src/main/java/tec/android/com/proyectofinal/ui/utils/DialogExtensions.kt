package tec.android.com.proyectofinal.ui.utils

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface

fun createDialog(
        context: Context,
        title: String = "",
        message: String = "Message",
        okBtn: String = context.getString(android.R.string.ok),
        noBtn: String = "",
        isCancelable: Boolean = true,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeListener: DialogInterface.OnClickListener? = null): AlertDialog {
    val dialog = AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(okBtn, positiveListener)
            .setCancelable(isCancelable)
    if (title.isNotEmpty()) {
        dialog.setTitle(title)
    }
    if (noBtn.isNotEmpty()) {
        dialog.setNegativeButton(noBtn, negativeListener)
    }
    dialog.create()
    return dialog.show()
}

fun loadingDialog(
        context: Context,
        title: String = "Loading",
        message: String = "Please wait"
): ProgressDialog =
        ProgressDialog.show(context, title,
                message, true, false)