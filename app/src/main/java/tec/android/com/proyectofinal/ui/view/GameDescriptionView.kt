package tec.android.com.proyectofinal.ui.view

import tec.android.com.proyectofinal.data.models.game_description.GameDescriptionResponse
import tec.android.com.proyectofinal.data.models.kart.KartItemModel

interface GameDescriptionView : BaseView {
    fun onGetGameDescription(response: MutableList<GameDescriptionResponse?>)
    fun onGetGameDescriptionError(error: Throwable)
    fun onGetFeaturedGames(list: MutableList<KartItemModel?>)
}